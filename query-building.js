const mongoose = require('mongoose')
const Building = require('./models/Building')
const Room = require('./models/Room')
mongoose.connect('mongodb://localhost:27017/example')
async function main () {
  // const room = await Room.findById('6224e36755932fe2161e5eac')
  // room.capacity = 20
  // await room.save()
  // console.log(room)
  const room = await Room.findOne({ capacity: { $gte: 100 } }).populate('building')
  console.log(room)
  console.log('----------')
  const rooms = await Room.find({ capacity: { $gt: 100 } }).populate('building')
  console.log(rooms)
  const buildings = await Building.find({})
  console.log(JSON.stringify(buildings))
}

main().then(() => {
  console.log('Finish')
})
